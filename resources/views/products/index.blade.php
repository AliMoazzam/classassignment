@extends('layout.layout')

@section('title', 'Product')

@section('content')

<div class="container">
  <h3 class="display-4">Product List</h3>
  <table class="table table-condensed">
    <thead>
      <tr>
        <td>Name</td>
        <td>Price</td>
        <td>Quantity</td>
        <td>Company Name</td>
        <td>Status</td>
        <td>Action</td>
      </tr>
    </thead>
    <tbody>
      @foreach($products as $product)
        <tr>
          <td> <a href="{{route('products.show', $product->id)}}"> {{$product->name}} </a></td>
          <td> {{$product->price}} </td>
          <td> {{$product->quantity}} </td>
          <td> {{$product->company_name}} </td>
          <td> 
            @if($product->isAvailable == true)
              Available
            @else
              Not Available
            @endif
           </td>
           <td>
            <form action="{{route('products.destroy',$product->id)}}" method="post">
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-outline-danger">Delete</button>
            </form>
           </td>
        </tr>
      @endforeach
    </tbody>
  </table>


</div>

@endsection