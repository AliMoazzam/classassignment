@extends('layout.layout')
@section('title', 'Edit')
@section('content')

<form method="post" action="/products/{{ $product->id }}">
@csrf
@method('PUT')
<div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control" value="{{$product->name}}">
        </div>
        <div class="form-group">
            <label for="price">Price</label>
            <input type="text" name="price" class="form-control" value="{{$product->price}}">
        </div>
        <div class="form-group">
            <label for="quantity">Quantity</label>
            <input type="text" name="quantity" class="form-control" value="{{$product->quantity}}">
        </div>
        <div class="form-group">
            <label for="company_name">Company Name</label>
            <input type="text" name="company_name" class="form-control" value="{{$product->company_name}}">
        </div>
        <div class="form-check">
            <input type="checkbox" id="isAvailable" name="isAvailable" value="{{$product->isAvailable}}">
            <label for="isAvailable"> Available</label>
        </div>
        <button type="submit" class="btn btn-outline-primary btn-bg">Update</button>
</form>

@endsection