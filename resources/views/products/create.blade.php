@extends('layout.layout')
@section('title', 'Create Product')
@section('content')
    <form method="post" action="/products">
    @csrf
    
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="price">Price</label>
            <input type="text" name="price" class="form-control">
        </div>
        <div class="form-group">
            <label for="quantity">Quantity</label>
            <input type="text" name="quantity" class="form-control">
        </div>
        <div class="form-group">
            <label for="company_name">Company Name</label>
            <input type="text" name="company_name" class="form-control">
        </div>
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="customCheck1" name="isAvailable">
            <label class="custom-control-label" for="customCheck1">isAvailable</label>
        </div>
        <button type="submit" class="btn btn-outline-primary btn-bg">Save</button>
    </form>
@endsection