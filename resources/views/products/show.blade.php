@extends('layout.layout')
@section('title', 'show')
@section('content')

<div class="container text-center">
    <div class="card">
        <div class="card-heading bg-primary"><h3>Product Details</h3></div>
        <div class="card-body bg-light">
            <p>Product Name: {{$product->name}}</p>
            <p>Product Price: {{$product->price}}</p>
            <p>Product Quantity: {{$product->quantity}}</p>
            <p>Product Company: {{$product->company_name}}</p>
            <p>Product Availability: <span class="bg-warning">{{$product->isAvailable == true? 'Available' : 'Not Available' }}</span></p>
        </div>
    </div>
</div>

@endsection